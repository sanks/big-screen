import cx from "classnames";
import './svgIcon.less';

interface Props {
  className: string;
  icon: string;
}

const SvgIcon = ({ className, icon, ...otherProps }: Props) => {
  return (
    <svg
      className={cx(
        "svgIcon",
        className
      )}
      aria-hidden="true">
      <use xlinkHref={'#icon-' + icon} />
    </svg>
  );
};


const customCache = new Set();

SvgIcon.createFromIconfontCN = (options: any) => {
  const { scriptUrl } = options;
  if (
    typeof document !== 'undefined' &&
    typeof window !== 'undefined' &&
    typeof document.createElement === 'function' &&
    typeof scriptUrl === 'string' &&
    scriptUrl.length &&
    !customCache.has(scriptUrl)
  ) {
    const script = document.createElement('script');
    script.setAttribute('src', scriptUrl);
    script.setAttribute('data-namespace', scriptUrl);
    customCache.add(scriptUrl);
    document.body.appendChild(script);
  }
};

export default SvgIcon;