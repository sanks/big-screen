import React from 'react';
import ReactECharts from 'echarts-for-react';

const Page: React.FC = () => {
  let obj = [{
      'community_name': 'K2平台',
      'planCharge': 25,
      'realCharge': 13
  }, {
      'community_name': '移动邮件',
      'planCharge': 25,
      'realCharge': 16
  }, {
      'community_name': '基础设施',
      'planCharge': 25,
      'realCharge': 18
  }, {
      'community_name': '智慧园区',
      'planCharge': 25,
      'realCharge': 19
  }, {
      'community_name': '物流地产',
      'planCharge': 25,
      'realCharge': 20
  }];
  let community_name = [];
  let planCharge = [];
  let realCharge = [];
  for (let i of obj) {
      community_name.push(i.community_name);
      planCharge.push(i.planCharge);
      realCharge.push(i.realCharge);
  }
  const options = {
    backgroundColor: "rgba(40, 47, 56, 0.5)",
    legend: {
        bottom: 45,
        textStyle: {
            color: '#fff',
        },
        data: ['实际成本', '预算成本']
    },
    grid: {
        left: '3%',
        right: '4%',
        top: '6%',
        bottom: '25%',
        containLabel: true
    },

    tooltip: {
        show: "true",
        trigger: 'axis',
        axisPointer: { // 坐标轴指示器，坐标轴触发有效
            type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    xAxis: {

        type: 'value',
        axisTick: {
            show: false
        },
        axisLine: {
            show: true,
            lineStyle: {
                color: '#fff',
            }
        },
        splitLine: {
            show: false
        },
    },
    yAxis: [{
            type: 'category',
            axisTick: {
                show: false
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#fff',
                }
            },
            data: community_name
        },
        {
            type: 'category',
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                show: false
            },
            splitArea: {
                show: false
            },
            splitLine: {
                show: false
            },
            data: community_name
        },

    ],
    series: [

        {
            name: '预算成本',
            type: 'bar',
            yAxisIndex: 1,
            barMaxWidth: 10,
            itemStyle: {
                normal: {
                    show: true,
                    color: '#277ace',
                    barBorderRadius: 50,
                    borderWidth: 0,
                    borderColor: '#333',
                }
            },
            barGap: '0%',
            barCategoryGap: '50%',
            data: planCharge
        },
        {
            name: '实际成本',
            type: 'bar',
            barMaxWidth: 10,
            itemStyle: {
                normal: {
                    show: true,
                    color: '#5de3e1',
                    barBorderRadius: 50,
                    borderWidth: 0,
                    borderColor: '#333',
                }
            },
            barGap: '0%',
            barCategoryGap: '50%',
            data: realCharge
        }

    ]
  };

  return <ReactECharts option={options} />;
};

export default Page;