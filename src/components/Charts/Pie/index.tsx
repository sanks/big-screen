import React, {useCallback, useRef} from 'react';
import ReactECharts, {EChartsInstance} from 'echarts-for-react';

let imgURL = "/asset/get/s/data-1612407787644-ElxYReRPV.png";
const color = [
    '#0CD2E6',
    '#3751E6',
    '#FFC722',
    '#886EFF',
    '#008DEC',
    '#114C90',
    '#00BFA5',
];
const data = [
    {
        "name": "A类型",
        "value": 30
    },
    {
        "name": "B类型",
        "value": 10
    },
    {
        "name": "C类型",
        "value": 15
    },
    {
        "name": "D类型",
        "value": 23
    },
    {
        "name": "E类型",
        "value": 10
    },
    {
        "name": "其他",
        "value": 12
    }
]
// 设置数据
function setChartOption(data: Array<any>) {
    const formatData: Array<any> = [];
    data.forEach(function(item, index) {
        formatData.push({
            value: item.value,
            name: item.name,
            itemStyle: {
                normal: {
                    borderWidth: 10,
                    shadowBlur: 20,
                    borderColor: color[index],
                    shadowColor: color[index],
                },
            },
        }, {
            value: 5,
            name: '',
            itemStyle: {
                normal: {
                    label: {
                        show: false,
                    },
                    labelLine: {
                        show: false,
                    },
                    color: 'rgba(0, 0, 0, 0)',
                    borderColor: 'rgba(0, 0, 0, 0)',
                    borderWidth: 0,
                },
            },
        })
    })

    return formatData;
};

const Page: React.FC = () => {
  const chartInstance = useRef<EChartsInstance>();
  const options = {
    backgroundColor: 'rgba(40, 47, 56, 0.5)',
    color: color,
    title:{
        text: '',
        top:20,
        left: 'center',
        textStyle: {
          fontSize: 20,
          color: 'rgba(0,141,236,0.9)',
        },
    },
    tooltip: {
        show: false,
        trigger: 'item',
        confine: true,
    },
    legend: {
        selectedMode: false,
        orient: 'vertical',
        left: 10,
        data: []
    },
    graphic: {
        elements: [{
            type: 'image',
            z: 3,
            style: {
                image: imgURL,
                width: 50,
                height: 50,
            },
            left: 'center',
            top: 'center',
        }, ],
    },
    series: [{
        name: '',
        type: 'pie',
        left: '0%',
        center: ['50%', '45%'],
        radius: ['50%', '52%'],
        hoverAnimation: true,
        data: setChartOption(data),
    }],
  };

  // 绑定事件
  const bind = useCallback((ref: EChartsInstance) => {
    if (!ref) return;
    ref.on('click', (params: any) => {
      console.log(params)
    });
  }, []);

  // 通过加载表成功的回调获取 echarts 实例
  const onChartReady = useCallback((ref: EChartsInstance) => {
    chartInstance.current = ref;
    bind(ref);

    // 自动循环突出放大功能
    let currentIndex = -1;

    // 取消所有高亮并显示当前图形
    const highlightPie = () => {

        // 取消之前高亮的图形
        for (let idx in options.series[0].data) {
            chartInstance.current._api.dispatchAction({
                type: 'downplay',
                seriesIndex: 0,
                dataIndex: idx
            });
        }
        
        // 高亮当前图形
        chartInstance.current._api.dispatchAction({
            type: 'highlight',
            seriesIndex: 0,
            dataIndex: currentIndex
        });
    }

    // 高亮效果切换到下一个图形
    const selectPie = () => {
        let dataLen = options.series[0].data.length;
        currentIndex = (currentIndex + 1) % dataLen;
        highlightPie();
    }
    
    let timeSetInterval = setInterval(selectPie, 2000);

    // 用户鼠标悬浮到某一图形时，停止自动切换并高亮鼠标悬浮的图形
    chartInstance.current._api.on('mouseover', function(e: any) {
        console.log('悬浮移入');
        clearInterval(timeSetInterval);
        currentIndex = e.dataIndex;
        highlightPie();
    });

    // 用户鼠标移出时，重新开始自动切换
    chartInstance.current._api.on('mouseout', function(e: any) {
        console.log('悬浮移出');
        if (timeSetInterval) {
            console.log('清除定时器');
            clearInterval(timeSetInterval);
        }
        // timeSetInterval = setInterval(selectPie, 2000);
        /* chartInstance.current._api.dispatchAction({
            type: 'highlight',
            seriesIndex: 1,
            dataIndex: 0
        }); */
    });
  }, [bind]);

   

  return <ReactECharts option={options} onChartReady={onChartReady}/>;
};

export default Page;