import { observer } from "mobx-react-lite";
import ParentSize from '@visx/responsive/lib/components/ParentSize';

import Example from './Example';
import './sandbox-styles.css';

  const root = observer(() => {
    return (<ParentSize>{({ width, height }) => <Example width={width} height={height} />}</ParentSize>)
  });

export default root;