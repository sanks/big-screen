import React from 'react';
import * as echarts from "echarts";
import ReactECharts from 'echarts-for-react';

const Page: React.FC = () => {
  let obj = [{
    'community_name': '中新国际城',
    'planCharge': 22,
    'realCharge': 9.9
  }, {
      'community_name': '鲁能泰山7号',
      'planCharge': 24,
      'realCharge': 19.9
  }, {
      'community_name': '盛世花城',
      'planCharge': 20,
      'realCharge': 12
  }, {
      'community_name': '唐城小区',
      'planCharge': 25,
      'realCharge': 18
  }, {
      'community_name': '黑虎泉印象',
      'planCharge': 20,
      'realCharge': 16
  }, {
      'community_name': '绿地城',
      'planCharge': 22,
      'realCharge': 8
  }];
  let community_name = [];
  let planCharge = [];
  let realCharge = [];
  for (let i of obj) {
      community_name.push(i.community_name);
      planCharge.push(i.planCharge);
      realCharge.push(i.realCharge);
  }
  const options = {
    backgroundColor: 'rgba(40, 47, 56, 0.5)',
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        top: '18%',
        right: '5%',
        left: '12%',
        bottom: '25%'
    },
    xAxis: [{
        type: 'category',
        data: ['移动开发', '流程IT', '硬件管理', '行政部', '审计部',],
        axisLine: {
            lineStyle: {
                color: 'rgba(255,255,255,0.12)'
            }
        },
        axisLabel: {
            interval: 0,
            margin: 10,
            color: '#e2e9ff',
            textStyle: {
                fontSize: 14
            },
        },
    }],
    yAxis: [{
        axisLabel: {
            formatter: '{value}',
            color: '#e2e9ff',
        },
        axisLine: {
            show: false
        },
        splitLine: {
            lineStyle: {
                color: 'rgba(255,255,255,0.12)'
            }
        }
    }],
    series: [{
        type: 'bar',
        data: [770, 550, 400, 355, 253],
        barWidth: '10px',
        itemStyle: {
            normal: {
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: 'rgba(0,244,255,1)' // 0% 处的颜色
                }, {
                    offset: 1,
                    color: 'rgba(0,77,167,1)' // 100% 处的颜色
                }], false),
                barBorderRadius: [30, 30, 30, 30],
                shadowColor: 'rgba(0,160,221,1)',
                shadowBlur: 4,
            }
        },
        label: {
            normal: {
                show: true,
                lineHeight: 24,
                width: 54,
                height: 22,
                backgroundColor: 'rgba(0,160,221,0.1)',
                borderRadius: 100,
                position: ['-8', '-50'],
                distance: 1,
                formatter: [
                    '    {d|●}',
                    '   {a|{c}}     \n',
                    '    {b|}'
                ].join(','),
                rich: {
                    d: {
                        color: '#3CDDCF',
                    },
                    a: {
                        color: '#fff',
                        align: 'center',
                        fontSize: 12,
                    },
                    b: {
                        width: 1,
                        height: 30,
                        borderWidth: 1,
                        borderColor: '#234e6c',
                        align: 'left'
                    },
                }
            }
        }
    }]
  };

  return <ReactECharts option={options} />;
};

export default Page;