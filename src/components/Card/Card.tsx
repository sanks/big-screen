import { observer } from "mobx-react-lite";
import './card.less';

interface Props {
  title: string;
  legends: string;
  children: any;
}

const Card = observer(({ title, legends, children}: Props) => {
  return (
    <div className="card">
      <div className="head">
        <div className="title">{title}</div>
        {legends || null}
      </div>
      <div className="content">{children}</div>
    </div>
  );
});

export default Card;