import { observer } from "mobx-react-lite";

interface StylesProps {
  readonly [index: string]: any
}

const styles: StylesProps = {
  root: {
    height: '30px',
    lineHeight: '30px',
  },
  label: {
    marginRight: '20px',
    fontSize: '10px',
    color: '#666',
  },
  circle: {
    display: 'inline-block',
    width: '12px',
    height: '12px',
    borderRadius: '6px',
    marginRight: '5px',
    position: 'relative',
    top: '2px',
  },
  line: {
    display: 'inline-block',
    width: '16px',
    height: 0,
    marginRight: '5px',
    borderBottom: 'none',
    verticalAlign: 'middle',
  },
};

interface Props {
  data: Array<any>;
  style: Object;
}

interface TProps {
  type: string;
  label: string;
  key: string;
  backgroundColor: string;
  border: string;
}

const Labels = observer(({ data, style}: Props) => {
  return (
    <div style={{ ...styles.root, ...style }}>
      {data.map((t: TProps) => {
        let tType = '';
        switch(t.type) {
          case 'root': 
            tType = 'root';
            break;
          case 'line':
            tType = 'line';
            break;
          case 'label':
            tType = 'label';
            break;
          case 'circle':
            tType = 'circle';
            break;
          default:
        }
        return (
          <span key={t.key} style={styles.label}>
            <i
              style={{ backgroundColor: t.backgroundColor, border: t.border, ...styles[tType] }}
            />
            {t.label}
          </span>
        );
      })}
    </div>
  );
});

export default Labels;