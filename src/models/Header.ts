import { types, Instance } from "mobx-state-tree";

export type HeaderModel = Instance<typeof Header>

export const Header = types
  .model({
    time: types.optional(types.number,  0),
  })
  .actions(self => ({
    setTime(time: number) {
      self.time = time
    },
    clear() {
      self.time = 0
    }
}))