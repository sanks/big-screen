/*
 * @Author: SKSSSX 1270256946@qq.com
 * @Date: 2023-02-25 23:07:00
 * @LastEditors: SKSSSX 1270256946@qq.com
 * @LastEditTime: 2023-03-05 07:18:14
 * @FilePath: /demo/react-mst/src/models/User.ts
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import { types, Instance } from "mobx-state-tree";

export type UserModel = Instance<typeof User>

export const User = types
  .model({
    id: types.string,
    name: types.string,
    messages: types.string
  })
  .actions(self => ({
    setUser(id: string, name: string, messages: string) {
      self.id = id;
      self.name = name;
      self.messages = messages;
    },
  }));