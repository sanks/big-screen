import RootStore, { RootStoreModel, RootStoreEnv } from "./RootStore";
import { Header } from "./Header";
import { MsgItems, MsgItemModel } from "./MsgItems";
import { User } from "./User";
import shortid from "shortid";
import { SnapshotIn } from "mobx-state-tree";

const msgItemData: SnapshotIn<MsgItemModel> = {
  id: shortid(),
  title: "示例标题",
  content: "示例内容"
}

export const createStore = (): RootStoreModel => {
  const msgItems = MsgItems.create({
    items: [msgItemData]
  });
  const header = Header.create({
    time: 0,
  });
  const user = User.create({
    id: "",
    name: "",
    messages: ""
  })

  const env: RootStoreEnv = { msgItems, user }

  const rootStore = RootStore.create(
    {
      header,
      msgItems,
      user
    },
    env
  )

  return rootStore
}
