import { types, Instance } from "mobx-state-tree"
import { Header } from "./Header"
import { MsgItems, MsgItemsModel } from "./MsgItems";
import { User, UserModel } from "./User";

export type RootStoreModel = Instance<typeof RootStore>
export type RootStoreEnv = {
  msgItems: MsgItemsModel,
  user: UserModel
}

const RootStore = types.model("RootStore", {
  header: Header,
  msgItems: MsgItems,
  user: User
})

export default RootStore