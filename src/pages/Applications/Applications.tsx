import { observer } from "mobx-react-lite";
import Card from '../../components/Card/Card';
import Circle from '../../components/Charts/Circle';
import './Applications.less';

const Applications = observer(() => {
  return (
    <Card title="部门分配率TOP5" legends="">
      <Circle />
    </Card>
  );
});

export default Applications;