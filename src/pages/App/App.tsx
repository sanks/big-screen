import React from 'react';
import Header from '../Header/Header';
import Loan from '../Loan/Loan';
import Distribution from '../Distribution/Distribution';
import Data from '../Data/Data';
import Tree from '../../components/Charts/Tree';
import Applications from '../Applications/Applications';
import Resource from '../Resources/Resource';
import Cost from '../Cost/Cost';
import User from '../User/User';

import './App.less';

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <Header/>
      </div>
      <div className="App-content">
        <div className="left">
          <div className="left-top">
            <Loan/>
          </div>
          <div className="left-center">
            <Distribution/>
          </div>
          <div className="left-bottom">
            <Resource/>
          </div>
        </div>
        <div className="center">
          <div className="center-top">
            <Data/>
          </div>
          <div className="center-bottom">
            <Tree/>
          </div>
        </div>
        <div className="right">
          <div className="right-top">
            <Applications/>
          </div>
          <div className="right-center">
            <Cost/>
          </div>
          <div className="right-bottom">
            <User/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
