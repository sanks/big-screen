import { observer } from "mobx-react-lite";
import SvgIcon from '../../components/SvgIcon/SvgIcon';
import './Data.less';

SvgIcon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_624956_exv4sjmfz1b.js',
});

const overview =  {
  custCount: 14695, // 客户数量
  loanCount: 19016, // 贷款笔数
  loanAmt: 63899, // 贷款金额
  overdueAmt: 2649125.65, // 逾期金额
  custCountComp: 10.6, // 客户比上月新增百分比
  loanCountComp: 9.39, // 贷款笔数你上月新增百分比
  loanAmtComp: 10.03, // 贷款金额比上月新增百分比
  overdueAmtComp: 1.0, // 逾期金额比上月降低百分比
};

const formatThousandthMoney = (s: any, n: any, defaultValue: any) => {
	if (typeof s === 'undefined') {
		return '0.00';
	}
	if (!s && typeof s !== 'undefined' && s !== 0 && defaultValue === undefined) {
		return '0.00';
	}
	if (!s && typeof s !== 'undefined' && s !== 0 && defaultValue !== undefined) {
		return defaultValue;
	}
	n = n > 0 && n <= 20 ? n : 0;
	// eslint-disable-next-line no-useless-escape
	s = parseFloat((s + '').replace(/[^\d\.-]/g, '')).toFixed(n) + '';
	let l = s.split('.')[0].split('').reverse();
	let r = s.split('.')[1];
	let t = '';
	for (let i = 0; i < l.length; i++) {
		t += l[i] + ((i + 1) % 3 === 0 && i + 1 !== l.length && l[i + 1] !== '-' ? ',' : '');
	}
	return t.split('').reverse().join('') + (n === 0 ? '' : ('.' + r));
};

const Data = observer(() => {
  return (
    <ul className="row">
      <li>
        <div className="title">
          <span>部门总数</span>
          <span className="percent">
            {''}
            [
            {/* <SvgIcon icon="icon-icon-caret-up" className="caretUpIcon" /> */}
            <i className="caret-up">上升 </i>
            {`${overview.custCountComp}%`}]
          </span>
        </div>
        <div className="content">{formatThousandthMoney(overview.custCount, 0, '')}</div>
      </li>
      <li>
        <div className="title">
          <span>应用总数</span>
          <span className="percent">
            {''}
            [
            {/* <SvgIcon icon="icon-icon-caret-up" className="caretUpIcon" /> */}
            <i className="caret-up">上升 </i>
            {`${overview.loanCountComp}%`}]
          </span>
        </div>
        <div className="content">{formatThousandthMoney(overview.loanCount, 0, '')}</div>
      </li>
      <li>
        <div className="title">
          <span>用户总数</span>
          <span className="percent">
            {''}
            [
            {/* <SvgIcon icon="icon-caret-down" className="caretDownIcon" /> */}
            <i className="caret-up">上升 </i>
            {`${overview.loanAmtComp}%`}]
          </span>
        </div>
        <div className="content">{formatThousandthMoney(overview.loanAmt, 0, '')}</div>
      </li>
      <li>
        <div className="title">
          <span>费用总数</span>
          <span className="percent">
            {''}
            [
            {/* <SvgIcon icon="icon-caret-down" className="caretDownIcon" /> */}
            <i className="caret-down">下降 </i>
            {`${overview.overdueAmtComp}%`}]
          </span>
        </div>
        <div className="content">{formatThousandthMoney(overview.overdueAmt, 2, '')}</div>
      </li>
    </ul>
  );
});

export default Data;