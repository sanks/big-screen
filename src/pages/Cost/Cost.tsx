import { observer } from "mobx-react-lite";
import Card from '../../components/Card/Card';
import Rank2 from '../../components/Charts/Rank2/index';

const Distribution = observer(() => {
  return (
    <Card title="部门成本TOP5" legends="">
      <Rank2 />
    </Card>
  );
});

export default Distribution;