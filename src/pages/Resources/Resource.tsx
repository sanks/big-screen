import { observer } from "mobx-react-lite";
import Card from '../../components/Card/Card';
import Pie from '../../components/Charts/Pie';

const Loan = observer(() => {
  return (
    <Card title="部门资源数量分布" legends="">
      <Pie />
    </Card>
  );
});


export default Loan;