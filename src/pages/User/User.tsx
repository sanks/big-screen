import { observer } from "mobx-react-lite";
import Card from '../../components/Card/Card';
import Rate from '../../components/Charts/Rate';

const Distribution = observer(() => {
  return (
    <Card title="部门用户数量TOP5" legends="">
      <Rate />
    </Card>
  );
});

export default Distribution;