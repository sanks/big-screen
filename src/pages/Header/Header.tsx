import { observer } from "mobx-react-lite";
import { RootStoreModel } from "../../models/RootStore";
import useInject from "../../hooks/useInject";
import SvgIcon from '../../components/SvgIcon/SvgIcon';
import { formatTime } from "../../utils/index";
import "./Header.less";

const mapStore = (rootStore: RootStoreModel) => ({ header: rootStore.header });

SvgIcon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_624956_br6r8nb9msp.js',
});

const Message = observer(() => {
  const { header } = useInject(mapStore);
  setInterval(() => {
    header.setTime(+new Date());
  }, 1000);
  return (
    <div className="header">
      <div className="time">
        {/* <SvgIcon icon="icon-time" className="timeIcon" /> */}
        {formatTime(4, header.time)}
      </div>
      <div className="title">部门总览大屏</div>
      <div className="desc">
        <SvgIcon icon="icon-shezhi" className="setIcon" />
        统计维度：昨天
      </div>
    </div>
  );
});

export default Message;