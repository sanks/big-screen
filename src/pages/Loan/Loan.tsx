import { observer } from "mobx-react-lite";
import Card from '../../components/Card/Card';
import Bar from '../../components/Charts/Bar';
import './Loan.less';

const Loan = observer(() => {
  return (
    <Card title="部门配额分布" legends="">
      <Bar />
    </Card>
  );
});


export default Loan;