import { observer } from "mobx-react-lite";
import Card from '../../components/Card/Card';
import Rank from '../../components/Charts/Rank';
import './Distribution.less';

const Distribution = observer(() => {
  return (
    <Card title="部门分配率TOP5" legends="">
      <Rank />
    </Card>
  );
});

export default Distribution;